# Use DPAPI to securely store password for daemon mode

[Crypt32 from Node](https://gitlab.com/TomasHubelbauer/bloggo-nodejs#crypt32)

This way the user name and password can be stored without worry they will get accessed.

After implementing this, this script can be run in an infinitely loop as a user-space process to keep checking and potentially alert the user or as a Windows service.
Use https://github.com/coreybutler/node-windows to install and uninstall the Windows service.
