import * as puppeteer from 'puppeteer';
import secrets from './secrets';

void async function() {
  const browser = await puppeteer.launch({ headless: false });
  const page = (await browser.pages())[0];
  await page.goto('https://mojedatovaschranka.cz');
  await page.type('#Ecom_User_ID', secrets.userName);
  await page.type('#Ecom_Password', secrets.accessKey);
  const enterA = await page.waitForSelector('#main > div.confirmation.pop-up > div.content > div.quadwide.col.t-c > p > a');
  await enterA.click();
  
  const messagesTable = await page.waitForSelector('table.messages');
  for (const lineTr of await messagesTable.$$('tr')) {
    const textTd = await lineTr.$('td.text');
    const senderTd = await lineTr.$('td.sender');
    
    const text = await page.evaluate((textTd: HTMLTableCellElement) => textTd.textContent, textTd);
    const sender = await page.evaluate((senderTd: HTMLTableCellElement) => senderTd.textContent, senderTd);
    console.log(`${sender}: ${text}`);
  }
}()
